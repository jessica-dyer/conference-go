import requests
import json
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_image(city: str):
    url = "https://api.pexels.com/v1/search"
    payload = {"query": city, "total_results": 1, "per_page": 1}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.request("GET", url, params=payload, headers=headers)
    image_dictionary = json.loads(response.content)
    image_url = image_dictionary["photos"][0]["url"]
    return image_url


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_geocoding_details(cityName: str) -> dict:
    url = "http://api.openweathermap.org/geo/1.0/direct"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    payload = {"q": cityName, "limit": 5, "appid": OPEN_WEATHER_API_KEY}
    response = requests.request("GET", url, params=payload, headers=headers)
    geocodingData = json.loads(response.content)
    geocodingDictionary = {
        "lon": geocodingData[0]["lon"],
        "lat": geocodingData[0]["lat"],
    }
    return geocodingDictionary


def get_weather_data(geocodingDictionary: dict):
    url = "http://api.openweathermap.org/data/2.5/weather"
    payload = {
        "lat": geocodingDictionary["lat"],
        "lon": geocodingDictionary["lon"],
        "units": "imperial",
        "APPID": OPEN_WEATHER_API_KEY,
    }
    response = requests.request("GET", url, params=payload)
    weatherData = json.loads(response.content)
    returnData = {
        "temp": weatherData["main"]["temp"],
        "description": weatherData["weather"][0]["description"],
    }
    return returnData


def fetchWeather(cityName: str):
    geocodingDictionary = get_geocoding_details(cityName)
    results = get_weather_data(geocodingDictionary)
    return results
