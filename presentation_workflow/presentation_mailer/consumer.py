import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    data = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f'{data["presenter_name"]}, we are happy to tell you that your presentation {data["title"]} has been accepted.',
        "admin@conference.go",
        [data["presenter_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    data = json.loads(body)
    send_mail(
        "Your presentation has not been accepted",
        f'{data["presenter_name"]}, we are very sorry to tell you that your presentation {data["title"]} has not been accepted.',
        "admin@conference.go",
        [data["presenter_email"]],
        fail_silently=False,
    )


while True:
    try:

        def main():
            print("inside main function")
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()

            # CODE FOR QUEUES
            channel.queue_declare(queue="process_approval")
            channel.basic_consume(
                queue="process_approval",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.queue_declare(queue="process_rejection")
            channel.basic_consume(
                queue="process_rejection",
                on_message_callback=process_rejection,
                auto_ack=True,
            )
            print(" [*] Waiting for messages. To exit press CTRL+C")
            channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
