from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.utils import timezone

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

while True:
    try:

        def update_account_VO(ch, method, properties, body):
            content = json.loads(body)
            print(content)
            print(
                "################### IM IN THE UPDATE ACCOUNT VO #####################"
            )
            email = content["email"]
            is_active = content["is_active"]
            print(content["updated"])
            content["updated"] = timezone.now()

            if is_active:
                AccountVO.objects.update_or_create(**content)
            else:
                AccountVO.objects.filter(email=email).delete()

        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()

            channel.exchange_declare(
                exchange="account_info", exchange_type="fanout"
            )
            result = channel.queue_declare(queue="", exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange="account_info", queue=queue_name)
            print(" [*] Waiting for messages. To exit press CTRL+C")
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=update_account_VO,
                auto_ack=True,
            )
            channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
